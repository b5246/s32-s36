const express=require("express")
const mongoose = require("mongoose")
const cors = require("cors")
const userRoutes = require("./routes/user")
const courseRoutes = require("./routes/course")
const app = express()

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended:true}))

mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.6vp9f.mongodb.net/S32-S36?retryWrites=true&w=majority",{
	useNewUrlParser:true,
	useUnifiedTopology:true
})


let db = mongoose.connection;
db.on("error",console.error.bind(console,"connection error"))
db.once("open",()=>console.log("We're connected to MongoDB Atlas"))

app.listen(process.env.PORT || 4000, ()=>{
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
})

app.use("/users", userRoutes)
app.use("/courses", courseRoutes)



//app.use("/", userRoutes)
//app.use("/register", userRoutes)




















