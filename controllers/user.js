const User = require("../models/User")
const Course = require("../models/Course")
const bcrypt = require("bcrypt")
const auth = require("../auth")

// Get all USER
module.exports.getAllUser = () => {
	return User.find({}).then(result => {
		return result;
	})
}

// Find by email
// GET: localhost:4000/users/checkEmail
module.exports.checkEmailExists = reqbody=>{
	return User.find({email: reqbody.email}).then(result=>{
		if(result.length>0){
			return true
		} else{
			return false
		}

	})
}

// CREATE new user
// POST: localhost:4000/users/register
module.exports.registerUser = requestBody => {
	let newUser = new User({
		firstName : requestBody.firstName,
		lastName: requestBody.lastName,
		email: requestBody.email,
		mobileNo: requestBody.mobileNo,
		// 10 is the no. of algorithms it will run to encrypt the password
		password: bcrypt.hashSync(requestBody.password,10),
		isAdmin: requestBody.isAdmin
	})
	/*for (let i = 0; i <= newUser.length;i++ ){
		if (newUser.firstName[i] == requestBody.firstName){
			
		}
	}*/
	// TODO if user exist
	return newUser.save().then((user, error) => {
		if(error){
			console.log(error)
			return false
		}
		else{
			return true
		}
	})
}

// User authentication (/login)
//POST : localhost:4000/users/login
module.exports.loginUser = reqbody=>{
	return User.findOne({email: reqbody.email}).then(result=>{
		// USer not exist
		if(result==null){
			return false;
		}else{
			//User exists
			const isPasswordCorrect = bcrypt.compareSync(reqbody.password,result.password)
			//password correct
			if(isPasswordCorrect){
				// ACCESS TOken create
				return{access:auth.createAccessToken(result)}
			} else{
				return false
			}
		}
	})
}

// ACTIVITY
// Solution 1
/*module.exports.getUserById = reqbody=>{
	return User.findById({_id: reqbody.id}).then((result,error)=>{
		if(result==null){
			console.log(error)
			return false
		} else{
			result.password = ""
			return result
		}

	})
}*/

//Discussion code
module.exports.getUserById = reqbody=>{
	return User.findById(reqbody.userId).then((result,error)=>{
			result.password = ""
			return result
	})
}

//************************************** WDC028-36 Express.js - API Development (Part 5)
// Contoller for enrolling the user to a specific course
module.exports.enroll = async (data)=>{
	let isUserUpdated = await User.findById(data.userId).then(user=>{
		// Add courseId in the user's enrollments array
		user.enrollments.push({courseId: data.courseId})
		// Save the updated user information in the database
		return user.save().then((user,error)=>{
			if(error){return false}
			else{return true}
		})
	})

	//add user id in the course's enrollees array
	let isCourseUpdated = await Course.findById(data.courseId).then(course=>{
		// Add userId in the course's enrollees array
		course.enrollees.push({userId:data.userId})
		//save updated course
		return course.save().then((course,error)=>{
			if(error){return false}
			else{return true}
		})
	})

	// Condition if the user and course doc have been updated successfully
	if(isUserUpdated && isCourseUpdated){return true}
	else{return false}

}
//**********************************************************************************










/* FROM POSTMAN AUTH CODE:
john 
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyYTFlMjVhMWM0ZjU2MDU2YTU1NTRhZCIsImVtYWlsIjoiam9nbkBtYWlsLmNvbSIsImlhdCI6MTY1NDg1NjQ0MX0.MiGyft-bRs8pJKt4cxJ4KMe2DSbpLTlVRDNUWwQ4UM0




admin (dona)
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyYTcyYWMwNTFhZDVmYmYyMmI4NDBjZiIsImVtYWlsIjoiRHNhYmFuQG1haWwuY29tIiwiaXNBZG1pbiI6dHJ1ZSwiaWF0IjoxNjU1MTI4MzM2fQ.sTESI6cAOQEQ744Wmqs-nvWqb2zINEHxqJlue7DfZU8


bruce (notadmin)
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyYTc0MjZiODE0YTA2OWIxYzI5NTNmNyIsImVtYWlsIjoiYmxAbWFpbC5jb20iLCJpc0FkbWluIjpmYWxzZSwiaWF0IjoxNjU1MTI4NzM1fQ.zo1GCibOpgzFnIR4GgNZpcSPaOB1YD_Ty7gLN_nb0RE
*/