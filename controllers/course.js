const Course = require("../models/Course")
const auth = require("../auth")

// Controller Function
// ADD /SAVE NEW COURSE
/*module.exports.addCourse = reqBody =>{
	let newCourse = new Course({
		name: reqBody.name,
		description : reqBody.description,
		price: reqBody.price
	})
	return newCourse.save().then((course,error)=>{
		if(error){
			console.log(error)
			return false
		}
		else{
			return true
		}
	})
}*/

//************************ACTIVITY 6/13/2022

// UPDATED SOLUTION TO ACTIVITY [WORKING]
module.exports.addCourse = (data) => {
	let newCourse = new Course({
		name : data.course.name,
		description : data.course.description,
		price : data.course.price
	})
	return newCourse.save().then((course, error) => {
		if (error) {return false}
		else {return true}
	})
}

/*
Sir. Alvin Solution
=======================
module.exports.addCourse = (data) => {

	// User is an admin
	if (data.isAdmin) {

		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newCourse = new Course({
			name : data.course.name,
			description : data.course.description,
			price : data.course.price
		});

		// Saves the created object to our database
		return newCourse.save().then((course, error) => {

			// Course creation failed
			if (error) {

				return false;

			// Course creation successful
			} else {

				return true;

			};

		});

	// User is not an admin
	} else {
		return Promise.resolve(false);
	};
	

};

*/

//****************************************************


module.exports.getAllCourses= () => {
	return Course.find({}).then(result => {
		return result
	})
}

module.exports.getCourse = reqBody =>{
	return Course.findById(reqBody.courseId).then(result=>{
		return result
	})
}

module.exports.updateCourse = (reqParams,reqBody)=>{
	let updatedCourse ={
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	return Course.findByIdAndUpdate(reqParams.courseId,updatedCourse).then((course,error)=>{
		if(error){return false}
		else {return true}
	})
}


//**************************************ACTIVITY 6/14/2022
/*module.exports.archiveCourse= (taskId,newContent)=>{
	return Course.findById(taskId).then((result,error)=>{
		if(error){
			console.log(error)
			return false
		}
		result.isActive = newContent.isActive
		return result.save().then((updateTask,saveErr)=>{
			if(saveErr){
				console.log(saveErr)
				return false
			} else{return updateTask}
		})
	})
}*/

// NOTE path changed to "/archive/:courseId" not "/:courseId" for later link is associated to updating course function
module.exports.archiveCourse= (reqParams,reqBody)=>{
	let archivedCourse = {isActive: reqBody.isActive }
	return Course.findByIdAndUpdate(reqParams.courseId,archivedCourse).then((course,error)=>{
		if(error){return false}
		else {return true}
	})
}

/*
Sir Alvin Solution
// [Activity] Controller function for archiving a course
module.exports.archiveCourse = (reqParams) => {

	let updateActiveField = {
		isActive : false
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) => {

		// Course not archived
		if (error) {

			return false;

		// Course archived successfully
		} else {

			return true;

		}

	});
};

*/

//***********************************************