const express = require("express")
const router = express.Router()
const courseController = require("../controllers/course")
const auth = require("../auth")

/*router.post("/",(req,res)=>{
	courseController.addCourse(req.body).then(resultFromController=>res.send(resultFromController))
})*/

/*router.post("/", auth.verify,(req,res)=>{
	const courseData = auth.decode(req.headers.authorization)
	courseController.addCourse(req.body).then(resultFromController=>res.send(resultFromController))
})*/

//************************ACTIVITY 6/13/2022

// UPDATED SOLUTION TO ACTIVITY [WORKING]
router.post("/", auth.verify, (req, res) => {
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(auth.decode(req.headers.authorization).isAdmin){
		courseController.addCourse(data).then(resultFromController => res.send(resultFromController))
	}else{res.send('Failed validation')}
})
/*
Sir Alvin Solution

// Route for retrieving specific course
router.get("/:courseId", (req, res) => {
	console.log(req.params.courseId);

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
})

*/
//*******************************************


router.get("/all",(req,res)=>{
	courseController.getAllCourses().then(resultFromController=>res.send(resultFromController))
})

router.get("/:courseId",(req,res)=>{
	console.log(req.params.courseId)
	courseController.getCourse(req.params).then(resultFromController=>res.send(resultFromController))
})

router.put("/:courseId",auth.verify,(req,res)=>{
	courseController.updateCourse(req.params, req.body).then(resultFromController=>res.send(resultFromController))
})


//*********************ACTIVITY 6/14/2022
// NOTE path changed to "/archive/:courseId" not "/:courseId" for later link is associated to updating course function

router.put("/:courseId/archive",auth.verify,(req,res)=>{
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(auth.decode(req.headers.authorization).isAdmin){
	courseController.archiveCourse(req.params, req.body).then(resultFromController=>res.send(resultFromController))
	}else{res.send('Failed validation')}
})

/*
Sir Alvin Solution
router.put("/:courseId/archive", auth.verify, (req, res) => {

	courseController.archiveCourse(req.params).then(resultFromController => res.send(resultFromController));
	
});

*/
//***********************************




module.exports = router
