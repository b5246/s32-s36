const express = require("express")
const router = express.Router()
const userController = require("../controllers/user")
const auth = require("../auth")

//route to Get all user
router.get("/", (req, res) => {
	userController.getAllUser().then(resultFromController => res.send(resultFromController));
})

// Route to GEt user by email
router.get("/checkEmail",(req,res)=>{
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
})
//Route for register new user authentication
router.post("/register",(req,res)=>{
	userController.registerUser(req.body).then(resultFromController=> res.send(resultFromController))
})

//Route for user authentication
router.post("/login", (req,res)=>{
	userController.loginUser(req.body).then(resultFromController=> res.send(resultFromController))
})

// ******************************************* Activit 6/10/2022
//My Version
/*router.get("/details", (req,res)=>{
	userController.getUserById(req.body).then(resultFromController=> res.send(resultFromController))
})*/


// with authorization integration (SIR ALVIN version)
/*router.get("/details", (req,res)=>{
	userController.getUserById({userId: userData.id}).then(resultFromController=> res.send(resultFromController))
})*/

// Route for retrieving user details
router.get("/details", auth.verify,(req,res)=>{
	const userData = auth.decode(req.headers.authorization)
	//Provide the user's ID for the getProfile controller method
	userController.getUserById({userId: userData.id}).then(resultFromController=> res.send(resultFromController))
})

//**************************************************************

//************************************** WDC028-36 Express.js - API Development (Part 5)
// ROUTE to Enroll user to a course
/*router.post("/enroll",(req,res)=>{
	let data = {
		userId: req.body.userId,
		courseId: req.body.courseId
	}
	userController.enroll(data).then(resultFromController =>{
		res.send(resultFromController)
	})
})*/

router.post("/enroll", auth.verify,(req,res)=>{
	let data = {
		userId: req.body.userId,
		courseId: req.body.courseId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(auth.decode(req.headers.authorization).isAdmin){
		userController.enroll(data).then(resultFromController =>{
			res.send(resultFromController)
		})
	}else{res.send('Failed validation')}
})

//**********************************************************************************




module.exports = router