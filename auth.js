const jwt = require("jsonwebtoken")

// User defined string data that will be used to create JSON web tokens
const secret = "CourseBookingAPI"

//Token creation
module.exports.createAccessToken= (user)=>{
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	return jwt.sign(data,secret,{})
}


//Token verification
module.exports.verify = (req,res,next)=>{
	// token > holds request header
	let token = req.headers.authorization
	// check if token is undefined
	if(typeof token!== "undefined"){
		console.log(token)
		token = token.slice(7,token.length)
		//Verify token > using secret code
		return jwt.verify(token,secret,(err,data)=>{
			if(err){
				return res.send({auth:"Authorization Failed"})
			}else{
				// Allows the application to proceed with the next middleware function/callback func in the route
				next()
			}
		})
	} else{
		return res.send({auth:"Verification Failed"})
	}
}

//Token decryption
module.exports.decode = token=>{
	if(typeof token !== "undefined"){
		// Retrieve only the token and removes the "bearer" prefix
		token = token.slice(7,token.length)
		return jwt.verify(token,secret,(err,data)=>{
			if(err){
				return null
			} else{
				// The "decode" method is used to obtain the information from JWT
				// The "{complete:true}" option allows us to return additional info from JWT
				// payload contains info provided in the "createAccessToken" method
				return jwt.decode(token,{complete:true}).payload
			}
		})
	}
}